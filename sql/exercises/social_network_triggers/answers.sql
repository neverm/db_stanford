-- Q1
-- Write a trigger that makes new students named 'Friendly' automatically like everyone else in their grade. That is, after the trigger runs, we should have ('Friendly', A) in the Likes table for every other Highschooler A in the same grade as 'Friendly'. 

CREATE TRIGGER Q1 AFTER INSERT ON Highschooler
WHEN new.name = 'Friendly'
BEGIN
	INSERT INTO Likes (ID1, ID2)
	SELECT new.ID, ID FROM Highschooler
	WHERE new.ID != ID AND new.grade = grade;
END;

-- Q2
-- Write one or more triggers to manage the grade attribute of new Highschoolers. If the inserted tuple has a value less than 9 or greater than 12, change the value to NULL. On the other hand, if the inserted tuple has a null value for grade, change it to 9. 

CREATE TRIGGER Q2_1 AFTER INSERT ON Highschooler
WHEN new.grade < 9 OR new.grade > 12
BEGIN
	UPDATE Highschooler SET grade = NULL WHERE ID=new.ID;
END;

CREATE TRIGGER Q2_2 AFTER INSERT ON Highschooler
WHEN new.grade is NULL
BEGIN
	UPDATE Highschooler SET grade = 9 WHERE ID=new.ID;
END;

-- Q3
-- Write one or more triggers to maintain symmetry in friend relationships. Specifically, if (A,B) is deleted from Friend, then (B,A) should be deleted too. If (A,B) is inserted into Friend then (B,A) should be inserted too. Don't worry about updates to the Friend table. 

CREATE TRIGGER Q3_1 AFTER DELETE ON Friend
BEGIN
	DELETE FROM Friend WHERE old.ID1 = ID2 AND old.ID2 = ID1;
END;

CREATE TRIGGER Q3_2 AFTER INSERT ON Friend
WHEN NOT EXISTS (SELECT * FROM Friend WHERE ID1 = new.ID2 and ID2 = new.ID1)
BEGIN
	INSERT INTO Friend (ID1, ID2) VALUES (new.ID2, new.ID1);
END;

-- Q4
-- Write a trigger that automatically deletes students when they graduate, i.e., when their grade is updated to exceed 12. 

CREATE TRIGGER Q4 AFTER UPDATE ON Highschooler
WHEN new.grade > 12
BEGIN
	DELETE FROM Highschooler WHERE ID = new.ID;
END;

-- Q5
-- Write a trigger that automatically deletes students when they graduate, i.e., when their grade is updated to exceed 12 (same as Question 4). In addition, write a trigger so when a student is moved ahead one grade, then so are all of his or her friends.

CREATE TRIGGER Q5 AFTER UPDATE ON Highschooler
WHEN (new.grade-1) = old.grade 
BEGIN
	UPDATE Highschooler SET grade=grade+1
	WHERE Highschooler.ID IN
	(SELECT ID2 FROM Friend WHERE ID1=new.ID);
END;

CREATE TRIGGER Q4 AFTER UPDATE ON Highschooler
WHEN new.grade > 12
BEGIN
	DELETE FROM Highschooler WHERE ID = new.ID;
END;

-- Q6
-- Write a trigger to enforce the following behavior:
-- If A liked B but is updated to A liking C instead, and B and C were friends,
-- make B and C no longer friends.
-- Don't forget to delete the friendship in both directions,
-- and make sure the trigger only runs when the "liked" (ID2) person is changed
-- but the "liking" (ID1) person is not changed. 
CREATE TRIGGER Q6 AFTER UPDATE ON Likes
WHEN old.ID1 = new.ID1
BEGIN
	DELETE FROM Friend
	WHERE Friend.ID1 = old.ID2 AND Friend.ID2 = new.ID2
	OR Friend.ID1 = new.ID2 AND Friend.ID2 = old.ID2;
END;
