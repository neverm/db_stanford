/**
Colleges paired with the highest GPA of their applicants
*/

select cName, state,
(select distinct  GPA as outerGPA
from Apply, Student
where College.cName = Apply.cName
and Apply.sID = Student.sID
and not exists  (select S.GPA as sGPA from Student S, Apply A
						  where S.sID = A.sID
						  and A.cName = College.cName
						  and  sGPA > outerGPA) 
) as GPA
from College


/**
* amount by which average cs applicant exceeds average non-cs applicant
*/
select CS.avgGPA - nonCS.avgGpa from
(select avg(GPA) as avgGPA from Student where sID in (select sID from Apply where major = 'CS')) as CS,
(select avg(GPA) as avgGPA from Student where sID not in (select sID from Apply where major = 'CS')) as nonCS


/**
* Number of applications by every student, including students that haven't applied to any college at all
*/

select Student.sID, Student.sName, count(distinct cName)
from Student, Apply
where Student.sID = Apply.sID
group by Student.sID
union
select S.sID, S.sName, 0
from Student S
where S.sID not in (select A.sID from Apply A)
